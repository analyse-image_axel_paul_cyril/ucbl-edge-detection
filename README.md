# UCBL-EDGE-DETECTION PACKAGE
In this package, you will find:

The filter DifferentialFilter.py file contains 2 differential filter: Kirsch and Sobel

The main.py file is the entry point of the program. It parses command arguments, and [load => filter => write] a video file accordingly.

Accordingly to this package, we provides some usage of this package :
`python3 -m ucbl-edge-detection -i path/to/input.mp4 -o path/to/output.mp4 "Sobel(5)"`
We can have anothers examples:
`python3 -m ucbl-edge-detection -i path/to/input.mp4 -o path/to/output.mp4 "Kirsch()"`

