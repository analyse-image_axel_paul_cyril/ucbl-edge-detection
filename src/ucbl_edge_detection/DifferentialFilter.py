from ucbl_space_filtering import Convolution
import numpy as np
import cv2

class Sobel(object):
    def __init__(self, dim):
        self.dim = dim

    def apply(self, frame):
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        grad = [
            cv2.Sobel(gray, cv2.CV_16S, 1, 0, ksize=self.dim, scale=1, delta=0, borderType=cv2.BORDER_DEFAULT),
            cv2.Sobel(gray, cv2.CV_16S, 0, 1, ksize=self.dim, scale=1, delta=0, borderType=cv2.BORDER_DEFAULT),
        ]
        grad = [cv2.convertScaleAbs(c) for c in grad]
        return cv2.addWeighted(grad[0], 0.5, grad[1], 0.5, 0)


#Define Kirsch convolution kernels
m1 = np.array([[ 5,  5,  5],
               [-3,  0, -3],
               [-3, -3, -3]])

m2 = np.array([[-3,  5,  5],
               [-3,  0,  5],
               [-3, -3, -3]])

m3 = np.array([[-3, -3, 5],
               [-3,  0, 5],
               [-3, -3, 5]])

m4 = np.array([[-3,-3,-3],
               [-3, 0, 5],
               [-3, 5, 5]])

m5 = np.array([[-3, -3, -3],
               [-3,  0, -3],
               [ 5,  5,  5]])

m6 = np.array([[-3, -3, -3],
               [ 5,  0, -3],
               [ 5,  5, -3]])

m7 = np.array([[5, -3, -3],
               [5,  0, -3],
               [5, -3, -3]])

m8 = np.array([[ 5,  5, -3],
               [ 5,  0, -3],
               [-3, -3, -3]])

kirsch_kernels = [m1, m2, m3, m4, m5, m6, m7, m8]
kirsch_filters = [Convolution(np.multiply(k, 1/15)) for k in kirsch_kernels]


class Kirsch(object):
    def apply(self, frame):
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        naf = np.asarray([k.apply(gray) for k in kirsch_filters])
        features = np.absolute(naf)
        return np.amax(features, 0)


def factory(definition):
    return eval(definition)
