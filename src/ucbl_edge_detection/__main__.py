import sys, getopt
import functools
import cv2


from ucbl_video_io import VideoReader, VideoWriter
from ucbl_edge_detection.DifferentialFilter import factory


def usage():
    print("Usage: \n -i, --input    select a file as input (image or video). \n -o, --output\
    the recipient file.\n -d        Debug mod.\n And, specified the differential filter you want in quotation marks. \n Filters : \n\
      Sobel(dim) (dim = the specified dimension of the kernel)\n      Kirsch()\n\n\
example : python3 -m ucbl-edge-detection -i myVideo.mp4 -o output.mp4 \"Sobel(5)\"" )


def read_args():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hg:i:o:d", ["help", "input=", "output="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    input_file_arg = None
    output_file_arg = None

    for op, arg in opts:
        if op in ("-h", "--help"):
            usage()
            sys.exit()
        elif op == '-d':
            global _debug
            _debug = 1
        elif op in ("-i", "--input"):
            input_file_arg = arg
        elif op in ("-o", "--output"):
            output_file_arg = arg

    return input_file_arg, output_file_arg, args


def main():
    if_arg, of_arg, filters_arg = read_args()
    print(if_arg)
    print(of_arg)
    print(filters_arg)

    if if_arg is None:
        raise AttributeError("Missing input (-i <input path>)/(--input=<input path>) argument")
    if of_arg is None:
        raise AttributeError("Missing output (-o <output path>)/(--output=<output path>) argument")

    filters = [factory(arg) for arg in filters_arg]

    with VideoReader(if_arg) as input_file:
        with VideoWriter(of_arg, input_file.get_meta()) as output_file:
            for frame in input_file.read():
                output_file.write(
                        cv2.cvtColor(
                                functools.reduce(lambda fr, fil: fil.apply(fr), filters, frame),
                                cv2.COLOR_GRAY2BGR))


if __name__ == "__main__":
    # execute only if run as a script
    main()
